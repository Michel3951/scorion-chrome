# Scorpion
Decided to implement my own features.

**Important**: For some reason someone added showdown to scorion. You must block this feature using an adblocker for this extension to work.

Add the following filter to your adblocker:
`https://cdn.rawgit.com/showdownjs/showdown/2.1.0/dist/showdown.min.js`

## Features
### Formviewer
- Adds a dark theme

### Check-in & out form
- Removes optional fields.
- Automatically sets the star rating to 3 stars.
- Automatically set working times to 8hrs and 0min.

### Productreview
- Automatically hides fields that are set to "Not applicable".
- Adds a markdown editor to all textareas (when creating and reviewing).
- Previews markdown when reviewing someones product (if the PR was made with this extension or the textarea value starts with `!md\n`).

## Suggestions
Feel free to [open an issue](https://gitlab.com/Michel3951/scorion-chrome/-/issues) if you have a feature request or have run into an issue.

## Installing the extension
This extension is made using Chrome's manifest V3, which means that it will work in any chromium based browser. Additionally support for Firefox has also been implemented.

### Chrome
You must first install dependencies with `npm install`. After this you can compile the app for production using `npm run build`, this will create the `dist` folder.

When you have the compiled the app, open up your browser and go to the extensions page (use the following in your address bar: `chrome:extensions`). On the top right you can enable 
developer mode which will add an extra toolbar on the top. Click on `Load unpacked` and select the `dist` folder.

### Firefox
I don't have Firefox, nor the knowledge of Firefox addons but you can build this app for Firefox using `npm run build:firefox`.

## Development 

This extension was created with [Extension CLI](https://oss.mobilefirst.me/extension-cli/)!


### Available Commands

| Commands | Description |
| --- | --- |
| `npm run start` | build extension, watch file changes |
| `npm run build` | generate release version |
| `npm run docs` | generate source code docs |
| `npm run clean` | remove temporary files |
| `npm run test` | run unit tests |
| `npm run sync` | update config files |

For CLI instructions see [User Guide &rarr;](https://oss.mobilefirst.me/extension-cli/)


## Screenshots
![PR](https://i.imgur.com/0Glwe8z.png)