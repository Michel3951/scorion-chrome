const d = document;

function fillCheckinForm() {
  /**
   * Automatically set the star rating to 3 stars (they're not my therapist)
   */
  const inputs = d.querySelectorAll('.starRating label');
  inputs[2].click();

  // Hide the disclaimer to save space
  document.querySelector('.ui-accordion').style.display = 'none';

  /**
   * What input fields should be hidden?
   * Useful for fields that are not required
   */
  const ignoreFields = [
    'Waarom voel je je zo?',
    'Welke eventuele hulpvragen heb je?',
    'Wat heb je vandaag geleerd?',
  ];

  /**
   * Loops over the array above and removes the text areas.
   * This loop also sets the "what did you learn" to .
   */
  d.querySelectorAll('tr td label').forEach((node) => {
    const id = node.attributes.getNamedItem('for')?.value;

    if (!id) return;

    const input = document.getElementById(id);

    if (node.textContent === 'Wat heb je vandaag geleerd?') {
      input.value = '.';
    }

    if (ignoreFields.includes(node.textContent)) {
      // eslint-disable-next-line no-param-reassign
      node.textContent = '';

      // First matches for showdown editor, second for default textarea
      const textArea = input.parentElement.querySelector('.custom-textarea') || input.parentElement.querySelector('textarea');
      if (textArea) {
        textArea.style.display = 'none';
      }
    }
  });

  /**
   * Last but not least, set the hours to 8 and minutes to 0
   */
  const [hours, minutes] = d.querySelectorAll('.clsFormElementBlock input');

  hours.value = 8;
  minutes.value = 0;

  /**
   * Hides the text at the bottom
   */
  Array.from(
    document.querySelectorAll('.clsFormPageContent .clsFormElementText'),
  ).reverse()[0].style.display = 'none';
}

module.exports = fillCheckinForm;
