const marked = require('marked');
const EasyMDE = require('easymde');

const d = document;
let blocks = [];

function isCreateForm() {
  return blocks.length === 8;
}

/**
 * Change fillable textareas to markdown fields
 */
function initializeMarkdownEditors() {
  d.querySelectorAll('.clsFormInputArea').forEach((node) => {
    if (node.hasAttribute('readonly') || node.hasAttribute('disabled')) return;
    const parent = node.parentElement;
    // Hide the original field
    node.style.display = 'none';

    // Create an element for the editor to attach
    const area = d.createElement('textarea');
    parent.appendChild(area);

    // Initialize the editor
    const editor = new EasyMDE({
      element: area,
      initialValue: node.value?.replace('!md\n', ''),
      minHeight: '50px',
      showIcons: [
        'bold', 'italic', 'strikethrough', 'heading', 'code', 'quote', 'image', 'link', 'ordered-list', 'unordered-list',
      ],
      hideIcons: ['guide', 'fullscreen', 'side-by-side'],
      status: false,
      spellChecker: false,
    });

    /**
     * When the editor changes value, we want to update the original textarea
     * so the data can be saved
     */
    editor.codemirror.on('change', () => {
      node.value = `!md\n${editor.value()}`;
    });
  });
}

function manageReviewForm() {
  /**
   * This runs over all the review fields and hides the one that are
   * marked as not applicable.
   */
  function hideCheckedBlocks() {
    blocks = Array.from(d.querySelectorAll('.clsFormElementBlock'));

    blocks.forEach((node, index) => {
      const box = node.nextElementSibling.querySelector('input[value=niet_van_toepassing]');

      if (box) {
        // eslint-disable-next-line no-param-reassign
        node.style.display = box.checked ? 'none' : 'block';

        if (isCreateForm()) blocks[index + 1].style.display = box.checked ? 'none' : 'block';
      }
    });
  }

  /**
   * Add event listeners to the "Not applicable" checkboxes
   */
  d.querySelectorAll('input[value=niet_van_toepassing]').forEach((node) => {
    node.addEventListener('change', () => {
      hideCheckedBlocks();
    });
  });

  /**
   * If the form is a review form, and not a review being created we want
   * to implement makrdown rendering.
   */
  if (!isCreateForm()) {
    // Loops over all readonly textareas
    d.querySelectorAll('textarea.clsFormInputArea').forEach((node) => {
      if (!node.hasAttribute('readonly') && !node.hasAttribute('disabled')) return;
      let text = node.textContent;
      if (!text.startsWith('!md\n')) return;
      text = text.replace('!md\n', '');

      const parent = node.parentElement;

      const div = d.createElement('div');
      div.classList.add('marked');

      // If the text content does not start with !md we don't render it as markdown

      node.style.display = 'none';
      div.innerHTML = marked.parse(text);

      parent.appendChild(div);
    });
  }

  hideCheckedBlocks();
}

function handleProductReview() {
  const url = window.location.search;

  /**
   * Check if the current page is the review form
   */
  if (url.startsWith('?2') || d.querySelector('.clsFormElementBlock')?.innerHTML.includes('Juiste kennis ontwikkelen')) {
    manageReviewForm();
  }

  initializeMarkdownEditors();
}

module.exports = handleProductReview;
