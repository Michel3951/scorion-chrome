const fillCheckinForm = require('./formviewer/checkin');
const handleProductReview = require('./formviewer/product-review');
const { modifyCss } = require('./style-injector');

const d = document;

const title = d.querySelector('.clsFormPageTitle');

if (title) {
  if (title.innerHTML.includes('-in')) {
    fillCheckinForm();
  } else if (title.innerHTML.includes('Productreview')) {
    handleProductReview();
  }

  modifyCss();
}
