const css = {
  background: '#212121',
  dark: '#303030',
  light: '#424242',
  textColor: '#dfe6e9',
  inputBackgroundColor: 'rgba(255, 255, 255, .5)',
  green: '#78e08f',
  linkColor: '#be2edd',
};

/**
 * This allows the app to modify the contents of the internal
 * iframe. We currently only change the styling.
 */
function injectoToFrames(frames) {
  frames.forEach((frame) => {
    frame.addEventListener('load', ({ target }) => {
      // Sadly the ScorionX forms are made this poor
      // that we have to use JS to override CSS
      // it is not possible to target the elements using css
      // because the styling is done inline 🤮
      const doc = target.contentDocument;
      const body = doc.querySelector('body');

      body.style.background = css.dark;

      doc.querySelectorAll('td').forEach((node) => {
        node.style.color = css.textColor;
        node.style.borderWidth = 0;
      });
    });
  });
}

function modifyCss() {
  // This part modifies the highchart styling in assesment forms
  // it cannot be overriden via css because of inline css.
  window.addEventListener('load', ({ target }) => {
    target.querySelectorAll('div[data-highcharts-chart]').forEach((node) => {
      const chart = node.querySelector('.highcharts-background');
      if (chart) {
        chart.setAttribute('fill', css.light);

        target.querySelectorAll('div[data-highcharts-chart] span').forEach((node) => {
          node.style.color = css.textColor;
        });
      }
    });
  });

  const iframes = document.querySelectorAll('iframe');

  if (iframes.length > 0) {
    injectoToFrames(iframes);
  }
}

module.exports = { modifyCss };
